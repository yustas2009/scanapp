import { ADD_SCANNED_TEXT } from '../types';

const scannedTexts = 'scannedTexts';


const initialState = {
  [scannedTexts]: []
};

export const textList = (state = initialState, action) => {
  switch (action.type) {
    case ADD_SCANNED_TEXT:
    return {
      ...state,
      [scannedTexts]: [...state.scannedTexts, action.payload]
    };
    default:
      return state;
  }
};
