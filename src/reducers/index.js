import { combineReducers } from 'redux';
import { textList } from './textList';
// import orderReducer, * as OrdersSelectors from './orderReducer.js';

const rootReducer = combineReducers({
  textList
});

export default rootReducer;
