import React from 'react';
import {
  View,
  Text,
  StyleSheet
} from 'react-native';
import Colors from '../theme/Colors';
import { screenWidth } from '../theme/styles';

const TextBlock = (props) => {
  const { scanDate, fullTextAnnotation } = props.scannedText;
  const { 
    container, 
    titleField, 
    dateStyles, 
    textField, 
    textColor 
  } = styles;
  
  return (
    <View style={container}>
      <View style={titleField}>
        <View>
          <Text>Date:</Text>
          <Text style={dateStyles}>{scanDate}</Text>
        </View>
      </View>
      <View style={textField}>
        <Text style={textColor}>{fullTextAnnotation}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    width: screenWidth - 30,
    borderRadius: 10,
    alignItems: 'center',
    backgroundColor: Colors.white,
    marginBottom: 15
  },
  titleField: {
    flexDirection: 'column',
    justifyContent: 'space-between',
    width: '25%',
    backgroundColor: Colors.white,
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderRadius: 1,
    borderLeftWidth: 5,
    borderColor: '#0f2e63',
  },
  textField: {
    width: '75%',
    backgroundColor: Colors.white,
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
    borderRadius: 1,
    borderLeftWidth: 1,
    borderColor: '#e0e0e0',
  },
  dateStyles: {
    fontSize: 12
  },
  textColor: {
    color: '#232323'
  }
});

export default TextBlock;
