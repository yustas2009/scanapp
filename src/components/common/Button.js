import React from 'react';
import { Button } from 'react-native';


export default class CaptureButton extends React.Component {
    render() {
        return (
                <Button 
                    onPress={this.props.onClick} 
                    disabled={this.props.buttonDisabled} 
                    title="Capture"
                    color="#e0067a"
                    accessibilityLabel="Learn more about this button" 
                />
        );
    }
}

