import React from 'react';
import {
  View,
} from 'react-native';

const Overlay = (props) => {
  const { children, hide } = props;
  if (hide) {
    return null;
  }
  return (
    <View {...this.props}>
      { children }
    </View>
  );
};

export default Overlay;
