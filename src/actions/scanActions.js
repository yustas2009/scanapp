import { ADD_SCANNED_TEXT } from '../types';

export const addText = (data) => {
  return (dispatch) => {
    dispatch({ type: ADD_SCANNED_TEXT, payload: data });
  };
};
