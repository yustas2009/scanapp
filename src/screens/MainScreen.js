import React from 'react';
import { ScrollView, StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { screenWidth } from '../theme/styles';
import TextBlock from '../components/textBlock'; 


class MainScreen extends React.Component {
    render() {
        const { container, addButtonField, addButtonStyles, buttonText, scrollBox } = styles;
        return (
          <View style={container}>
            <View style={addButtonField}>
              <TouchableOpacity
                style={addButtonStyles}
                onPress={() => this.props.navigation.navigate('CameraScreen')}
                underlayColor='#fff'
              >
                <Text style={buttonText}>SCAN NEW TEXT</Text>
              </TouchableOpacity>
            </View>
            <ScrollView style={scrollBox}>
              {
                this.props.textList.scannedTexts && 
                  this.props.textList.scannedTexts.map((item, index) => {
                    return (
                      <TextBlock scannedText={item} key={index} />
                    );
                })
              }
            </ScrollView>
          </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      backgroundColor: '#0f2e63',
    },
    addButtonField: {
      height: 80,
      width: screenWidth,
      paddingHorizontal: 10,
      backgroundColor: '#0f2e63',
      justifyContent: 'center',
      alignItems: 'center'
    },
    addButtonStyles: {
      height: 50,
      width: screenWidth - 100,
      backgroundColor: '#e0067a',
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: 10
    },
    buttonText: {
      fontSize: 20,
      color: '#fff'
    },
    scrollBox: {
      flex: 1,
      flexDirection: 'column',
      paddingHorizontal: 15
    }
});

const mapStateToProps = (state) => {
  return {
    textList: state.textList,
  };
};

export default connect(mapStateToProps, {})(MainScreen);
