import React, { Component } from 'react';
import { View, Text, Dimensions, Alert, StyleSheet, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import { RNCamera } from 'react-native-camera';
import { addText } from '../actions/scanActions';
import moment from 'moment';
import style, { screenHeight, screenWidth } from '../theme/styles';
import CaptureButton from '../components/common/Button';
import config from '../../config';
import { DATE_FORMAT } from '../types';


class CameraScreen extends React.Component { 
    constructor(props){
        super(props);
        this.state = { 
            identifedAs: '',
            loading: false
        }
    }

    takePicture = async function(){
        if (this.camera) {
                this.setState((previousState, props) => ({
                    loading: true
                }));

                // Set options
                const options = {
                    base64: true
                };
                // // Pause the camera's preview
                // this.camera.pausePreview();

                // Get the base64 version of the image
                const data = await this.camera.takePictureAsync(options);

                // Get the identified image
                this.identifyImage(data.base64);
        }
    }

    identifyImage = (imageData) => {
        this.checkForLabels(imageData).then((data) => {
            const buildScannedObj = this.buildScannedObject(data.responses[0]);
 
            this.props.addText(buildScannedObj);
            this.props.navigation.navigate('Home');
        }).catch(() => {
            this.errorAlert();
        });
    }

    // API call to google cloud
    checkForLabels = async (base64) => {
        return await
            fetch(config.googleCloud.api + config.googleCloud.apiKey, {
                method: 'POST',
                body: JSON.stringify({
                    "requests": [
                        {
                            "image": {
                                "content": base64
                            },
                            "features": [
                                {
                                    "type": "TEXT_DETECTION"
                                }
                            ]
                        }
                    ]
                })
            }).then((response) => {
                return response.json();
            }, () => {
                this.errorAlert();
            });
    }

    buildScannedObject = (responses) => {
        const scannedTextResponse = {
            scanDate: moment(new Date()).format(DATE_FORMAT),
            fullTextAnnotation: responses.fullTextAnnotation.text,
            // textAnnotations: responses.textAnnotations
        }
        return scannedTextResponse;
    }

    errorAlert = () => {
        Alert.alert("Probably you didn't scanned text or your connection is down...");
        this.props.navigation.navigate('Home');
    }
    
    render() {
        const { preview, loadingIndicator, buttonBlock } = styles;
        return (
            <RNCamera ref={ref => {this.camera = ref;}} style={preview}>
                <ActivityIndicator size="large" style={loadingIndicator} color="#fff" animating={this.state.loading} />
                <View style={buttonBlock}>
                    {
                        this.state.loading && <Text style={{ color: '#fff' }}>Processing...</Text>
                    }
                    <CaptureButton buttonDisabled={this.state.loading} onClick={this.takePicture.bind(this)} />
                </View>
            </RNCamera>
        );
    }
}

const styles = StyleSheet.create({
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
        height: Dimensions.get('window').height,
        width: Dimensions.get('window').width,
    },
    loadingIndicator: {
        flex: 1,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        height: Dimensions.get('window').height,
        width: Dimensions.get('window').width,
    },
    buttonBlock: {
        padding: 20
    }
});


export default connect(null, { addText })(CameraScreen);
