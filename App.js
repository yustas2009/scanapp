import React, { Component } from 'react';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import { Provider } from 'react-redux';
import configureStore from './src/configureStore';
import CameraScreen from './src/screens/CameraScreen';
import MainScreen from './src/screens/MainScreen';


const store = configureStore();

type Props = {};
export default class App extends Component<Props> {
  render() {
    return (
      <Provider store={store}>
        <MainApp />
      </Provider>
    );
  }
}

const FadeTransition = (index, position) => {
  const sceneRange = [index - 1, index];
  const outputOpacity = [0.9, 1];
  const transition = position.interpolate({
    inputRange: sceneRange,
    outputRange: outputOpacity
  });

  return {
    opacity: transition
  };
};

const NavigationConfig = () => {
  return {
    screenInterpolator: (sceneProps) => {
      const position = sceneProps.position;
      // const scene = sceneProps.scene;
      const index = sceneProps.index;

      return FadeTransition(index, position);
    }
  };
};

const RootStack = createStackNavigator({
  Home: {
    screen: MainScreen,
    headerMode: 'none',
    header: null,
    navigationOptions: {
        header: null
    }
  },
  CameraScreen: {
    screen: CameraScreen,
    headerMode: 'none',
    header: null,
    navigationOptions: {
        header: null
    }
  },
},
{
  initialRouteName: 'Home',
  transitionConfig: NavigationConfig,
  navigationOptions: {
      headerStyle: {
        height: 50,
        backgroundColor: '#f4511e'
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
        lineHeight: 23
      },
    },
});

const MainApp = createAppContainer(RootStack);

